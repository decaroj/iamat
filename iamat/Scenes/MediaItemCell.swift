//
//  MediaItemCell.swift
//  iamat
//
//  Created by Daniel Esteban Cardona Rojas on 7/24/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class MediaItemCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func render(item: MediaItem) {
        itemNameLabel.text = item.caption ?? "Nothing here"
    }

}
