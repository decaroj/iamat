//
//  MediaListViewController.swift
//  iamat
//
//  Created by Daniel Esteban Cardona Rojas on 7/24/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit
import Alamofire

class MediaListViewController: UIViewController {


    @IBOutlet weak var tableView: UITableView!
    var items: [MediaItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataManager.getMediaItems {[unowned self] (response: DataResponse<[MediaItem]>) in
            guard let items = response.result.value else {
                print(response)
                return
            }
            self.items = items.filter { $0.type == "image" }
            self.tableView.reloadData()
        }
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


// MARK: - TableViewDelegate
extension MediaListViewController : UITableViewDelegate {
    
}

// MARK: - TableViewDataSource
extension MediaListViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediaItemCell")! as! MediaItemCell
        let item = items[indexPath.row]

        cell.render(item: item)
        DataManager.image(for: item, completion: { response in
            if let image = response.result.value {
                cell.itemImageView.image = image
            }
        })
        
        return cell
    }
}
