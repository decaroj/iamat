//
//  MediaItem.swift
//  iamat
//
//  Created by Daniel Esteban Cardona Rojas on 7/24/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import Foundation


struct MediaItem: Codable {
    let caption: String?
    let type: String
    let base: ResourceBase?
    
    var folderPath: URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent("MediaItems")
        return fileURL
    }
    
    var localPath: URL {
       return folderPath.appendingPathComponent(base!.fullFileName)
    }

}

struct ResourceBase: Codable {
    let filename: String
    let basePath: String
    let ext: String
    let width: Int
    let height: Int
    
    var fullFileName: String {
        let size = "small"
        return [filename, size, ext].joined(separator: ".")
    }
    var resourcePath: URL {
        return URL(string:  basePath)!.appendingPathComponent(fullFileName)
    }
}
