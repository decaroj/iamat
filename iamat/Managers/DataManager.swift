//
//  MediaListPresenter.swift
//  iamat
//
//  Created by Daniel Esteban Cardona Rojas on 7/24/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import Alamofire
import CodableAlamofire
import AlamofireImage

class DataManager {
    static func getMediaItems(completion: @escaping ((DataResponse<[MediaItem]>) -> Void)) {
        let apiUrl = "https://api.iamat.com/atcodes/viacom-porta-qa/media"
        Alamofire.request(apiUrl).responseDecodableObject { (response: DataResponse<[MediaItem]>) in
            completion(response)
        }
    }
    
    static func downloadImage(for item: MediaItem) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (item.localPath, [.removePreviousFile, .createIntermediateDirectories])
        }
        if let base = item.base {
            Alamofire.download(base.resourcePath, to: destination)
        }
    }
    
    static func image(for item: MediaItem, completion: @escaping ((DataResponse<Image>) -> Void)) {
        guard let base = item.base else {
            return
        }
        
        Alamofire.request(base.resourcePath).responseImage { (response: DataResponse<Image>) in
            completion(response)
        }
    }
}
